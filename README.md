# ReactJS/Redux - Corporate Website

  

Official Website of www.csyo-az.org made with React and Redux

## Usage  

``` bash
# install dependencies
$ npm install
# serve with hot reload at localhost:3000
$ npm start
# serve with hot reload and Sass watch at localhost:7000
$ npm run dev
# production build
$ npm run build

```
## API URL
You can change API URL from ./src/index.js :

    const  axiosInstance  =  axios.create({
    	baseURL:  'YOUR_API_URL'
    });

