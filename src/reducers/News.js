import { GET_NEWS } from '../actions/Url';

export default (state = null, action) => {
    switch (action.type) {
        case GET_NEWS:
            return action.payload;
        default:
            return state;
    }
};