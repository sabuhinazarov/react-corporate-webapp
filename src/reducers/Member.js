import { GET_MEMBER } from '../actions/Url';

export default (state = null, action) => {
    switch (action.type) {
        case GET_MEMBER:
            return action.payload;
        default:
            return state;
    }
};