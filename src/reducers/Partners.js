import { GET_PARTNERS } from '../actions/List';

export default (state = [], action) => {
    switch (action.type) {
        case GET_PARTNERS:
            return action.payload.data;
        default:
            return state;
    }
};