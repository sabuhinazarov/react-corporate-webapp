import { GET_ANNOUNCE } from '../actions/Url';

export default (state = null, action) => {
    switch (action.type) {
        case GET_ANNOUNCE:
            return action.payload;
        default:
            return state;
    }
};