import { GET_MEMBERS } from '../actions/List';

export default (state = [], action) => {
    switch (action.type) {
        case GET_MEMBERS:
            return action.payload.data;
        default:
            return state;
    }
};