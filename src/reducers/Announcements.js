import { GET_ANNOUNCEMENTS, MORE_ANNOUNCE } from '../actions/List';

export default (state = [], action) => {
    switch (action.type) {
        case GET_ANNOUNCEMENTS:
            return action.payload.data;
        case MORE_ANNOUNCE:
            return state.concat(action.payload.data);
        default:
            return state;
    }
};