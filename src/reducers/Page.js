import { GET_PAGE } from '../actions/Url';

export default (state = null, action) => {
    switch (action.type) {
        case GET_PAGE:
            return action.payload;
        default:
            return state;
    }
};