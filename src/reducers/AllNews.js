import { GET_ALLNEWS, MORE_NEWS } from '../actions/List';

export default (state = [], action) => {
    switch (action.type) {
        case GET_ALLNEWS:
            return action.payload.data;
        case MORE_NEWS:
            return state.concat(action.payload.data);
        default:
            return state;
    }
};