import { GET_SLIDES } from '../actions/List';

export default (state = [], action) => {
    switch (action.type) {
        case GET_SLIDES:
            return action.payload.data;
        default:
            return state;
    }
};