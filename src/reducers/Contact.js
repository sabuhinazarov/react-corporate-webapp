import { SEND_MESSAGE } from '../actions/Url';

export default (state = null, action) => {
    switch (action.type) {
        case SEND_MESSAGE:
            return action.payload;
        default:
            return state;
    }
};