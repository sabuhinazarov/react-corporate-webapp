import { combineReducers } from 'redux';
import Page from './Page';
import AllNews from './AllNews';
import News from './News';
import Announcements from './Announcements';
import Announce from './Announce';
import Members from './Members';
import Member from './Member';
import Partners from './Partners';
import Slides from './Slides';
import Contact from './Contact';

export default combineReducers({
    page: Page,
    allnews: AllNews,
    news: News,
    announcements: Announcements,
    announce: Announce,
    members: Members,
    member: Member,
    partners: Partners,
    slides: Slides,
    contact: Contact
});