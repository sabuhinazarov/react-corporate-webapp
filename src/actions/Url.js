//Get single page by url
export const GET_PAGE = 'GET_PAGE';
export const getPage = (url) => async (dispatch, getState, api) => {
    const pageData = {
        urlpath: url
    };
    const res = await api.post('/page/url', pageData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_PAGE,
        payload: res.data
    });
};


//Get single member by url
export const GET_MEMBER = 'GET_MEMBER';
export const getMember = (url) => async (dispatch, getState, api) => {
    const memberData = {
        urlpath: url
    };
    const res = await api.post('/team/url', memberData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_MEMBER,
        payload: res.data
    });
};

//Get single news by url
export const GET_NEWS = 'GET_NEWS';
export const getNews = (url) => async (dispatch, getState, api) => {
    const newsData = {
        urlpath: url
    };
    const res = await api.post('/news/url', newsData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_NEWS,
        payload: res.data
    });
};

//Get single announce by url
export const GET_ANNOUNCE = 'GET_ANNOUNCE';
export const getAnnounce = (url) => async (dispatch, getState, api) => {
    const annData = {
        urlpath: url
    };
    const res = await api.post('/announce/url', annData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_ANNOUNCE,
        payload: res.data
    });
};

//Send contact message
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const sendMessage = (contactData) => async (dispatch, getState, api) => {
    const res = await api.post('/contact', contactData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: SEND_MESSAGE,
        payload: res.data
    });
};