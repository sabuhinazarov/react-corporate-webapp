//Get all news
export const GET_ALLNEWS = 'GET_ALLNEWS';
export const getAllNews = (skip, limit) => async (dispatch, getState, api) => {
    const newsdataData = {
        skip,
        limit
    };
    const res = await api.post('/news/all', newsdataData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_ALLNEWS,
        payload: res.data
    });
};

//Get more news
export const MORE_NEWS = 'MORE_NEWS';
export const MoreNews = (skip, limit) => async (dispatch, getState, api) => {
    const newsdataData = {
        skip,
        limit
    };
    const res = await api.post('/news/all', newsdataData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: MORE_NEWS,
        payload: res.data
    });
};

//Get all announcements
export const GET_ANNOUNCEMENTS = 'GET_ANNOUNCEMENTS';
export const getAnnouncements = (skip, limit) => async (dispatch, getState, api) => {
    const annData = {
        skip,
        limit
    };
    const res = await api.post('/announce/all', annData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_ANNOUNCEMENTS,
        payload: res.data
    });
};

//Get all news
export const MORE_ANNOUNCE = 'MORE_ANNOUNCE';
export const moreAnnounce = (skip, limit) => async (dispatch, getState, api) => {
    const annData = {
        skip,
        limit
    };
    const res = await api.post('/announce/all', annData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: MORE_ANNOUNCE,
        payload: res.data
    });
};


//Get all team member by type
export const GET_MEMBERS = 'GET_MEMBERS';
export const getMembers = (type, skip, limit) => async (dispatch, getState, api) => {
    const memData = {
        skip,
        limit
    };
    const res = await api.post('/team/all/' + type, memData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_MEMBERS,
        payload: res.data
    });
};


//Get all partners
export const GET_PARTNERS = 'GET_PARTNERS';
export const getPartners = (skip, limit) => async (dispatch, getState, api) => {
    const ptData = {
        skip,
        limit
    };
    const res = await api.post('/partner/all', ptData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_PARTNERS,
        payload: res.data
    });
};

//Get all Slides
export const GET_SLIDES = 'GET_SLIDES';
export const getSlides = (skip, limit) => async (dispatch, getState, api) => {
    const slData = {
        skip,
        limit
    };
    const res = await api.post('/slide/all', slData)
        .then(function (res) {
            return res;
        })
        .catch(function (err) {
            return err.response;
        });

    dispatch({
        type: GET_SLIDES,
        payload: res.data
    });
};