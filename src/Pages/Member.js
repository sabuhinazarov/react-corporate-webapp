import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import {getMember} from '../actions/Url';
import NotFound from '../Components/NotFound';
import Error from '../Components/Error';
import LoadingComponent from '../Components/Loading';
import {GlobalTranslation} from "../translation/Fields";

class Member extends Component{
    componentDidMount(){
        this.props.getMember("member/" + this.props.match.params.urlpath);
    }
    head() {
        const lang = this.props.match.params.lang;
        let PageTitle;
        let PageDesc;
        if(lang === "en"){
            PageTitle = this.props.member.name_en + " | " + GlobalTranslation.GlobalEN.brandName;
            PageDesc = this.props.member.bio_en.replace(/(<([^>]+)>)/ig,"").substr(0, 320);
        }else{
            PageTitle = this.props.member.name + " | " + GlobalTranslation.GlobalAZ.brandName;
            PageDesc = this.props.member.bio.replace(/(<([^>]+)>)/ig,"").substr(0, 320)
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageDesc} />
                <meta property="og:description" content={PageDesc} />
                <meta name="twitter:description" content={PageDesc} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/${this.props.member.urlpath}`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/${this.props.member.urlpath}`} />
            </Helmet>
        );
    }
    render(){
        const member = this.props.member;
        const lang = this.props.match.params.lang;
        if(member && member.code === 200) {
            const HeaderImage = "http://csyo-az.org/wp-content/uploads/2016/08/xeber-14.08.2016-1200x800.jpg";
            return(
                <div className="team-page">
                    {this.head()}
                    <div className="team-header" style={{backgroundImage: `url(${HeaderImage})`}}>
                        {lang === "en" ? (
                            <div className="container">
                                <h1>{member.name_en}</h1>
                                <h2>{member.position_en}</h2>
                            </div>
                        ):(
                            <div className="container">
                                <h1>{member.name}</h1>
                                <h2>{member.position}</h2>
                            </div>
                        )}
                    </div>
                    <div className="member-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-12 col-sm-4 col-lg-3">
                                    <div className="member-left">
                                        <img src={member.photo} alt={member.name} className="img-responsive"/>
                                        <ul className="member-social">
                                            <li>
                                                <a href={member.facebook} target="_blank">
                                                    <i className="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href={member.twitter} target="_blank">
                                                    <i className="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href={member.linkedin} target="_blank">
                                                    <i className="fa fa-linkedin"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href={"mailto:" + member.email}>
                                                    <i className="fa fa-envelope"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-8 col-lg-9">
                                    {lang === "en" ? (
                                        <div className="member-bio" dangerouslySetInnerHTML={{__html: member.bio_en}}>

                                        </div>
                                    ):(
                                        <div className="member-bio" dangerouslySetInnerHTML={{__html: member.bio}}>

                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }else if(member && member.code === 404){
            return(
                <NotFound/>
            );
        }else if(!member){
            return (
                <LoadingComponent />
            );
        }else{
            return (
                <Error />
            );
        }
    }
}

function mapStateToProps(globalState) {
    return {
        member: globalState.member
    };
}

export default connect(mapStateToProps, {getMember})(Member);