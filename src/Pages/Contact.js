import React, {Component} from 'react';
import { connect } from 'react-redux';
import {sendMessage} from '../actions/Url';
import { Helmet } from 'react-helmet';
import {GlobalTranslation, ContactTranslation} from "../translation/Fields";

class Contact extends Component{
    constructor(props){
        super(props);

        this.state = {
            name: null,
            email: null,
            subject: null,
            message: null,
            msgContent: null,
            msgType: null,
        };
        this.sendIT = this.sendIT.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e){
        this.setState({ [e.target.name]: e.target.value.replace(/(<([^>]+)>)/ig,"")});
    }

    sendIT(e){
        e.preventDefault();
        if(!this.state.name || !this.state.email || !this.state.subject || !this.state.message){
            this.setState({msgContent: "Zəhmət olmazsa, bütün xanaları doldurun."});
            this.setState({msgType: "alert alert-danger"});
        }else{
            this.setState({msgContent: null});
            this.setState({msgType: null});
            this.props.sendMessage(this.state);
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps && nextProps.contact){
            if(nextProps.contact.code === 200){
                this.setState({msgContent: "Mesajınız göndərildi. Ən qısa zamanda sizinlə əlaqə saxlayacağıq"});
                this.setState({msgType: "alert alert-success"});
                this.setState({name: null});
                this.setState({email: null});
                this.setState({subject: null});
                this.setState({message: null});
            }
            else{
                this.setState({msgContent: nextProps.contact.error});
                this.setState({msgType: "alert alert-danger"});
            }
        }
    }
    head() {
        const lang = this.props.match.params.lang;
        let PageTitle;
        if(lang === "en"){
            PageTitle = ContactTranslation.EN.PageTitle + " | " + GlobalTranslation.GlobalEN.brandName;
        }else{
            PageTitle = ContactTranslation.AZ.PageTitle + " | " + GlobalTranslation.GlobalAZ.brandName;
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageTitle} />
                <meta property="og:description" content={PageTitle} />
                <meta name="twitter:description" content={PageTitle} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/contact`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/contact`} />
            </Helmet>
        );
    }
    render(){
        const HeaderImage = "http://csyo-az.org/wp-content/uploads/2016/08/Biz%C9%99-qo%C5%9Ful.jpg";
        const lang = this.props.match.params.lang;
        let HomeTitle;
        let address;
        let ContactFields;
        if(lang === "en"){
            HomeTitle = GlobalTranslation.GlobalEN.HomeTitle;
            address = GlobalTranslation.GlobalEN.address;
            ContactFields = ContactTranslation.EN;
        }else{
            HomeTitle = GlobalTranslation.GlobalAZ.HomeTitle;
            address = GlobalTranslation.GlobalAZ.address;
            ContactFields = ContactTranslation.AZ;
        }
        return(
          <div className="contact-page">
              <div className="contact-header" style={{backgroundImage: `url(${HeaderImage})`}}>
                  <div className="container">
                      <h1>{ContactFields.PageTitle}</h1>
                      <ul className="breadcrumb">
                          <li><a href="/">{HomeTitle}</a></li>
                          <li>{ContactFields.PageTitle}</li>
                      </ul>
                  </div>
              </div>
              <div className="contact-content">
                  <div className="container">
                      <div className="row">
                          <div className="col-sm-12 col-md-8 col-lg-9">
                              <h2>{ContactFields.contactForm}</h2>

                              {this.state.msgContent &&
                              <p className={this.state.msgType}>
                                  {this.state.msgContent}
                              </p>
                              }
                              <form className="contact-form" onSubmit={this.sendIT}>
                                  {/*<div id="success">*/}
                                      {/*<div role="alert" className="alert alert-success">*/}
                                          {/*<strong>Thanks</strong> for using our template. Your message has been sent.*/}
                                      {/*</div>*/}
                                  {/*</div>*/}
                                  <div className="row">
                                      <div className="col-sm-6">
                                          <div className="form-field">
                                              <input
                                                  className="input-sm form-full"
                                                  onChange={this.onChange}
                                                  value={this.state.name}
                                                  name="name"
                                                  placeholder={ContactFields.name}
                                                  type="text" />
                                          </div>
                                          <div className="form-field">
                                              <input
                                                  className="input-sm form-full"
                                                  onChange={this.onChange}
                                                  value={this.state.email}
                                                  name="email"
                                                  placeholder="Email"
                                                  type="text" />
                                          </div>
                                          <div className="form-field">
                                              <input
                                                  className="input-sm form-full"
                                                  onChange={this.onChange}
                                                  value={this.state.subject}
                                                  name="subject"
                                                  placeholder={ContactFields.subject}
                                                  type="text" />
                                          </div>
                                      </div>
                                      <div className="col-sm-6">
                                          <div className="form-field">
                                              <textarea
                                                  className="form-full"
                                                  rows="7"
                                                  onChange={this.onChange}
                                                  value={this.state.message}
                                                  name="message"
                                                  placeholder={ContactFields.message}></textarea>
                                          </div>
                                      </div>
                                      <div className="col-sm-12 mt-30">
                                          <button className="btn-text" type="submit" id="submit" name="button">
                                              {ContactFields.buttonText}
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <div className="col-sm-12 col-md-4 col-lg-3">
                              <h2>{ContactFields.contactInfo}</h2>
                              <div className="contact-info">
                                  <ul className="info">
                                      <li>
                                          <i className="fa fa-map-marker"></i>
                                          <div className="content">
                                              {address}
                                          </div>
                                      </li>

                                      <li>
                                          <i className="fa fa-phone"></i>
                                          <div className="content">
                                              <p>
                                                  +994 55 823 72 90
                                              </p>
                                          </div>
                                      </li>
                                      <li>
                                          <i className="fa fa-envelope"></i>
                                          <div className="content">
                                              <p>
                                                  office@csyo-az.org
                                              </p>
                                          </div>
                                      </li>
                                  </ul>
                                  <ul className="event-social">
                                      <li>
                                          <a href="https://www.facebook.com/sdgt.csyo" target="_blank" rel="noopener noreferrer">
                                            <i className="fa fa-facebook" aria-hidden="true"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="https://twitter.com/SDGTCSYO" target="_blank" rel="noopener noreferrer">
                                            <i className="fa fa-twitter" aria-hidden="true"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="https://www.youtube.com/user/SDGTcsyo" target="_blank" rel="noopener noreferrer">
                                            <i className="fa fa-youtube-play" aria-hidden="true"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="https://www.instagram.com/sdgt.csyo/" target="_blank" rel="noopener noreferrer">
                                            <i className="fa fa-instagram" aria-hidden="true"></i>
                                          </a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

function mapStateToProps(globalState) {
    return {
        contact: globalState.contact
    };
}

export default connect(mapStateToProps, {sendMessage})(Contact);