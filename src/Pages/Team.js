import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import {getMembers} from '../actions/List';
import PeopleGrid from '../Components/PeopleGrid';
import {TeamTranslation, GlobalTranslation} from '../translation/Fields';

class Team extends Component{
    componentDidMount(){
        this.props.getMembers(this.props.match.params.urlpath, 0, 50);
    }
    head() {
        const lang = this.props.match.params.lang;
        const type = this.props.match.params.urlpath;
        let PageTitle;
        if(lang === "en"){
            if(type === "secretariat"){
                PageTitle = TeamTranslation.TeamEN.secretariatTitle + " | " + GlobalTranslation.GlobalEN.brandName;
            }else{
                PageTitle = TeamTranslation.TeamEN.boardTitle + " | " + GlobalTranslation.GlobalEN.brandName;
            }
        }else{
            if(type === "secretariat"){
                PageTitle = TeamTranslation.TeamAZ.secretariatTitle + " | " + GlobalTranslation.GlobalAZ.brandName;
            }else{
                PageTitle = TeamTranslation.TeamAZ.boardTitle + " | " + GlobalTranslation.GlobalAZ.brandName;
            }
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageTitle} />
                <meta property="og:description" content={PageTitle} />
                <meta name="twitter:description" content={PageTitle} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/team/${type}`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/team/${type}`} />
            </Helmet>
        );
    }
    render(){
        const lang = this.props.match.params.lang;
        const type = this.props.match.params.urlpath;
        let pageTitle;
        let HomeTitle;
        if(lang === "en"){
            if(type === "secretariat"){
                pageTitle = TeamTranslation.TeamEN.secretariatTitle;
            }else{
                pageTitle = TeamTranslation.TeamEN.boardTitle;
            }
            HomeTitle = GlobalTranslation.GlobalEN.HomeTitle;
        }else{
            if(type === "secretariat"){
                pageTitle = TeamTranslation.TeamAZ.secretariatTitle;
            }else{
                pageTitle = TeamTranslation.TeamAZ.boardTitle;
            }
            HomeTitle = GlobalTranslation.GlobalAZ.HomeTitle;
        }
        const HeaderImage = "http://csyo-az.org/wp-content/uploads/2016/08/xeber-14.08.2016-1200x800.jpg";
        return(
            <div className="team-page">
                {this.head()}
                <div className="team-header" style={{backgroundImage: `url(${HeaderImage})`}}>
                    <div className="container">
                        <h1>{pageTitle}</h1>
                        <ul className="breadcrumb">
                            <li><a href={"/" + lang}>{HomeTitle}</a></li>
                            <li>{pageTitle}</li>
                        </ul>
                    </div>
                </div>
                <div className="member-list">
                    <div className="container">
                        <PeopleGrid members={this.props.members} lang={lang}/>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(globalState) {
    return {
        members: globalState.members
    };
}

export default connect(mapStateToProps, {getMembers})(Team);