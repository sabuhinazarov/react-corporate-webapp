import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import {getPage} from '../actions/Url';
import NotFound from '../Components/NotFound';
import Error from '../Components/Error';
import LoadingComponent from '../Components/Loading';
import {GlobalTranslation} from '../translation/Fields';

class Static extends Component{
    componentDidMount(){
        this.props.getPage("page/" + this.props.match.params.urlpath);
    }
    head() {
        const lang = this.props.match.params.lang;
        let PageTitle;
        let PageDesc;
        if(lang === "en"){
            PageTitle = this.props.page.title_en + " | " + GlobalTranslation.GlobalEN.brandName;
            PageDesc = this.props.page.content_en.replace(/(<([^>]+)>)/ig,"").substr(0, 320);
        }else{
            PageTitle = this.props.page.title + " | " + GlobalTranslation.GlobalAZ.brandName;
            PageDesc = this.props.page.content.replace(/(<([^>]+)>)/ig,"").substr(0, 320)
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageDesc} />
                <meta property="og:description" content={PageDesc} />
                <meta name="twitter:description" content={PageDesc} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/${this.props.page.urlpath}`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/${this.props.page.urlpath}`} />
            </Helmet>
        );
    }
    render(){
        const page = this.props.page;
        const lang = this.props.match.params.lang;
        let pageTitle;
        let pageContent;
        let HomeTitle;

        if(page && page.code === 200) {
            const HeaderImage = page.imageUrl;
            ////Multi Lang
            if(lang === "en"){
                pageTitle = page.title_en;
                pageContent = page.content_en;
                HomeTitle = GlobalTranslation.GlobalEN.HomeTitle;
            }else{
                pageTitle = page.title;
                pageContent = page.content;
                HomeTitle = GlobalTranslation.GlobalAZ.HomeTitle;
            }
            return(
                <div className="static-page">
                    {this.head()}
                    <div className="static-header" style={{backgroundImage: `url(${HeaderImage})`}}>
                        <div className="container">
                            <h1>{pageTitle}</h1>
                            <ul className="breadcrumb">
                                <li><a href={"/" + lang}>{HomeTitle}</a></li>
                                <li>{pageTitle}</li>
                            </ul>
                        </div>
                    </div>
                    <div className="static-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div dangerouslySetInnerHTML={{__html: pageContent}}>

                                    </div>
                                </div>
                            </div>
                            {page.jotform &&
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="iframe-box">
                                        <iframe src={page.jotform} frameBorder="0" name="63126017562450" id="63126017562450" title="jotform"></iframe>
                                    </div>
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            );
        }else if(page && page.code === 404){
            return(
                <NotFound/>
            );
        }else if(!page){
            return (
                <LoadingComponent />
            );
        }else{
            return (
                <Error />
            );
        }
    }
}

function mapStateToProps(globalState) {
    return {
        page: globalState.page
    };
}

export default connect(mapStateToProps, {getPage})(Static);