import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import axios from 'axios';
import {getAllNews, getAnnouncements, getPartners, getSlides} from '../actions/List';

import Slider from '../Components/Slider/Slider';
import Partners from '../Components/Partners';
import NewsBlock from '../Components/NewsBlock';
// import InstaCollage from '../Components/InstaCollage';
import {HomeTranslation} from '../translation/Fields';

class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            instaimages: []
        };
        this.getInsta = this.getInsta.bind(this);
    }
    getInsta (){
        axios.get(`http://instafeed.sabuhi.com/getjson/github`)
            .then(res => {
                this.setState({instaimages: res.data.images})
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    componentDidMount(){
        this.props.getAllNews(0, 3);
        this.props.getAnnouncements(0, 3);
        this.props.getPartners(0, 50);
        this.props.getSlides(0, 10);
        // this.getInsta();
    }
    head() {
        const currentPath = this.context.router.route.location.pathname;
        const lang = currentPath.split('/')[1];
        let PageTitle;
        if(lang === "en"){
            PageTitle = "Common Sense Youth Organization | Home Page";
        }else{
            PageTitle = "Sağlam Düşüncə Gənclər Təşkilatı | Ana Səhifə";
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageTitle} />
                <meta property="og:description" content={PageTitle} />
                <meta name="twitter:description" content={PageTitle} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}`} />
            </Helmet>
        );
    }
    render(){
        const currentPath = this.context.router.route.location.pathname;
        const lang = currentPath.split('/')[1];
        let HomeFields;
        if(lang === "en"){
            HomeFields = HomeTranslation.HomeEN;
        }else{
            HomeFields = HomeTranslation.HomeAZ;
        }

        return(
          <main>
              {this.head()}
              <Slider slides={this.props.slides} lang={lang}/>
              <NewsBlock title={HomeFields.newsBlockTitle} preurl="/news" buttonText={HomeFields.redmoreText} contents={this.props.allnews} lang={lang}/>

              {/*<InstaCollage images={this.state.instaimages} />*/}
              <NewsBlock title={HomeFields.annBlockTitle} preurl="/announcements" buttonText={HomeFields.redmoreText} contents={this.props.announcements} lang={lang}/>
              <Partners title={HomeFields.partnersTitle} partners={this.props.partners} lang={lang}/>
          </main>
        );
    }
}

function mapStateToProps(globalState) {
    return {
        allnews: globalState.allnews,
        announcements: globalState.announcements,
        partners: globalState.partners,
        slides: globalState.slides
    };
}

Home.contextTypes = {
    router: PropTypes.object.isRequired
};

export default connect(mapStateToProps, {getAllNews, getAnnouncements, getPartners, getSlides})(Home);