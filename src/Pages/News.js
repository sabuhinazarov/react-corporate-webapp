import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import {getNews} from '../actions/Url';
import NotFound from '../Components/NotFound';
import Error from '../Components/Error';
import LoadingComponent from '../Components/Loading';
import {GlobalTranslation} from "../translation/Fields";
import Moment from 'moment';

class News extends Component{
    componentDidMount(){
        this.props.getNews("news/" + this.props.match.params.urlpath);
    }
    head() {
        const lang = this.props.match.params.lang;
        let PageTitle;
        let PageDesc;
        if(lang === "en"){
            PageTitle = this.props.news.title_en + " | " + GlobalTranslation.GlobalEN.brandName;
            PageDesc = this.props.news.content_en.replace(/(<([^>]+)>)/ig,"").substr(0, 320);
        }else{
            PageTitle = this.props.news.title + " | " + GlobalTranslation.GlobalAZ.brandName;
            PageDesc = this.props.news.content.replace(/(<([^>]+)>)/ig,"").substr(0, 320);
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageDesc} />
                <meta property="og:description" content={PageDesc} />
                <meta name="twitter:description" content={PageDesc} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/${this.props.news.urlpath}`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/${this.props.news.urlpath}`} />
            </Helmet>
        );
    }
    render(){
        const news = this.props.news;
        const lang = this.props.match.params.lang;
        let newsTitle;
        let newsContent;
        let allnewsTitle;
        let HomeTitle;
        let published;

        if(news && news.code === 200) {
            if(lang === "en"){
                newsTitle = news.title_en;
                newsContent = news.content_en;
                HomeTitle = GlobalTranslation.GlobalEN.HomeTitle;
                allnewsTitle = GlobalTranslation.GlobalEN.AllNewsTitle;
                published = GlobalTranslation.GlobalEN.published;
            }else{
                newsTitle = news.title;
                newsContent = news.content;
                HomeTitle = GlobalTranslation.GlobalAZ.HomeTitle;
                allnewsTitle = GlobalTranslation.GlobalAZ.AllNewsTitle;
                published = GlobalTranslation.GlobalAZ.published;
            }
            return(
                <div className="static-page">
                    {this.head()}
                    <div className="static-header" style={{backgroundImage: `url(${news.imageUrl})`}}>
                        <div className="container">
                            <h1>{newsTitle}</h1>
                            <ul className="breadcrumb">
                                <li><a href={"/" + lang}>{HomeTitle}</a></li>
                                <li><a href={"/" + lang + "/news"}>{allnewsTitle}</a></li>
                                <li>{newsTitle}</li>
                            </ul>
                        </div>
                    </div>
                    <div className="static-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <img src={news.imageUrl} alt={news.title} className="img-responsive mb-2" />
                                    <div dangerouslySetInnerHTML={{__html: newsContent}}>

                                    </div>
                                    <ul className="meta-fields">
                                        <li>{published}: {Moment(news.created).format('DD.MM.YYYY')}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }else if(news && news.code === 404){
            return(
                <NotFound/>
            );
        }else if(!news){
            return (
                <LoadingComponent />
            );
        }else{
            return (
                <Error />
            );
        }
    }
}

function mapStateToProps(globalState) {
    return {
        news: globalState.news
    };
}

export default connect(mapStateToProps, {getNews})(News);