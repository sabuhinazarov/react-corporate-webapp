import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import {getAnnouncements, moreAnnounce} from '../actions/List';
import NewsGrid from '../Components/NewsGrid';
import {GlobalTranslation} from "../translation/Fields";

class Announcements extends Component{
    constructor(props) {
        super(props);
        this.state = {
            skip: 0,
            perPage: 24,
            inPage: 24
        };
        this.loadMore = this.loadMore.bind(this);
    }
    loadMore(){
        this.setState({skip: this.state.skip + this.state.perPage, inPage: this.state.inPage + this.state.perPage}, function () {
            this.props.moreAnnounce(this.state.skip , this.state.perPage);
        });
    }
    componentDidMount(){
        this.props.getAnnouncements(this.state.skip , this.state.perPage);
    }
    head() {
        const lang = this.props.match.params.lang;
        let PageTitle;
        if(lang === "en"){
            PageTitle = GlobalTranslation.GlobalEN.AnnouncementsTitle + " | " + GlobalTranslation.GlobalEN.brandName;
        }else{
            PageTitle = GlobalTranslation.GlobalAZ.AnnouncementsTitle + " | " + GlobalTranslation.GlobalAZ.brandName;
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageTitle} />
                <meta property="og:description" content={PageTitle} />
                <meta name="twitter:description" content={PageTitle} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/announcements`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/announcements`} />
            </Helmet>
        );
    }
    render(){
        const HeaderImage = "http://csyo-az.org/wp-content/uploads/2016/08/Biz%C9%99-qo%C5%9Ful.jpg";
        const lang = this.props.match.params.lang;
        const announcements = this.props.announcements;

        let HomeTitle;
        let PageTitle;
        let redmoreText;
        let loadMore;
        let loadmoreText;

        if(lang === "en"){
            HomeTitle = GlobalTranslation.GlobalEN.HomeTitle;
            PageTitle = GlobalTranslation.GlobalEN.AnnouncementsTitle;
            redmoreText = GlobalTranslation.GlobalEN.redmoreText;
            loadmoreText = GlobalTranslation.GlobalEN.loadmoreText;
        }else{
            HomeTitle = GlobalTranslation.GlobalAZ.HomeTitle;
            PageTitle = GlobalTranslation.GlobalAZ.AnnouncementsTitle;
            redmoreText = GlobalTranslation.GlobalAZ.redmoreText;
            loadmoreText = GlobalTranslation.GlobalAZ.loadmoreText;
        }

        loadMore = announcements.length === this.state.inPage;

        return(
          <div className="contact-page">
              {this.head()}
              <div className="contact-header" style={{backgroundImage: `url(${HeaderImage})`}}>
                  <div className="container">
                      <h1>{PageTitle}</h1>
                      <ul className="breadcrumb">
                          <li><a href={"/" + lang}>{HomeTitle}</a></li>
                          <li>{PageTitle}</li>
                      </ul>
                  </div>
              </div>
              <div className="contact-content">
                  <div className="container">
                      <div className="row">
                          <NewsGrid buttonText={redmoreText} allnews={announcements} lang={lang}/>
                      </div>
                      <div className="row">
                          {loadMore &&
                          <div className="col-xs-12 load-more">
                              <button className="btn-text" onClick={this.loadMore}>
                                  {loadmoreText}
                              </button>
                          </div>
                          }
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

function mapStateToProps(globalState) {
    return {
        announcements: globalState.announcements
    };
}

export default connect(mapStateToProps, {getAnnouncements, moreAnnounce})(Announcements);