import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import {getAnnounce} from '../actions/Url';
import NotFound from '../Components/NotFound';
import Error from '../Components/Error';
import LoadingComponent from '../Components/Loading';
import {GlobalTranslation} from "../translation/Fields";
import Moment from "moment/moment";

class Announce extends Component{
    componentDidMount(){
        this.props.getAnnounce("announce/" + this.props.match.params.urlpath);
    }
    head() {
        const lang = this.props.match.params.lang;
        let PageTitle;
        let PageDesc;
        if(lang === "en"){
            PageTitle = this.props.announce.title_en + " | " + GlobalTranslation.GlobalEN.brandName;
            PageDesc = this.props.announce.content_en.replace(/(<([^>]+)>)/ig,"").substr(0, 320);
        }else{
            PageTitle = this.props.announce.title + " | " + GlobalTranslation.GlobalAZ.brandName;
            PageDesc = this.props.announce.content.replace(/(<([^>]+)>)/ig,"").substr(0, 320)
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageDesc} />
                <meta property="og:description" content={PageDesc} />
                <meta name="twitter:description" content={PageDesc} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/${this.props.announce.urlpath}`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/${this.props.announce.urlpath}`} />
            </Helmet>
        );
    }
    render(){
        const announce = this.props.announce;
        const lang = this.props.match.params.lang;
        let announceTitle;
        let announceContent;
        let allannTitle;
        let HomeTitle;
        let published;

        if(announce && announce.code === 200) {
            if(lang === "en"){
                announceTitle = announce.title_en;
                announceContent = announce.content_en;
                HomeTitle = GlobalTranslation.GlobalEN.HomeTitle;
                allannTitle = GlobalTranslation.GlobalEN.AnnouncementsTitle;
                published = GlobalTranslation.GlobalEN.published;
            }else{
                announceTitle = announce.title;
                announceContent = announce.content;
                HomeTitle = GlobalTranslation.GlobalAZ.HomeTitle;
                allannTitle = GlobalTranslation.GlobalAZ.AnnouncementsTitle;
                published = GlobalTranslation.GlobalAZ.published;
            }
            return(
                <div className="static-page">
                    {this.head()}
                    <div className="static-header" style={{backgroundImage: `url(${announce.imageUrl})`}}>
                        <div className="container">
                            <h1>{announceTitle}</h1>
                            <ul className="breadcrumb">
                                <li><a href={"/" + lang}>{HomeTitle}</a></li>
                                <li><a href={"/" + lang + "/announcements"}>{allannTitle}</a></li>
                                <li>{announceTitle}</li>
                            </ul>
                        </div>
                    </div>
                    <div className="static-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <img src={announce.imageUrl} alt={announce.title} className="img-responsive mb-2" />
                                    <div dangerouslySetInnerHTML={{__html: announceContent}}>

                                    </div>
                                    <ul className="meta-fields">
                                        <li>{published}: {Moment(announce.created).format('DD.MM.YYYY')}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }else if(announce && announce.code === 404){
            return(
                <NotFound/>
            );
        }else if(!announce){
            return (
                <LoadingComponent />
            );
        }else{
            return (
                <Error />
            );
        }
    }
}

function mapStateToProps(globalState) {
    return {
        announce: globalState.announce
    };
}

export default connect(mapStateToProps, {getAnnounce})(Announce);