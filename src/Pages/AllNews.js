import React, {Component} from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import {getAllNews} from '../actions/List';
import NewsGrid from '../Components/NewsGrid';
import {GlobalTranslation} from "../translation/Fields";

class AllNews extends Component{
    componentDidMount(){
        this.props.getAllNews(0, 24);
    }
    head() {
        const lang = this.props.match.params.lang;
        let PageTitle;
        if(lang === "en"){
            PageTitle = GlobalTranslation.GlobalEN.AllNewsTitle + " | " + GlobalTranslation.GlobalEN.brandName;
        }else{
            PageTitle = GlobalTranslation.GlobalAZ.AllNewsTitle + " | " + GlobalTranslation.GlobalAZ.brandName;
        }
        return (
            <Helmet>
                <title>{PageTitle}</title>
                <meta property="og:title" content={PageTitle} />
                <meta name="twitter:title" content={PageTitle} />
                <meta name="description" content={PageTitle} />
                <meta property="og:description" content={PageTitle} />
                <meta name="twitter:description" content={PageTitle} />
                <meta property="og:url" content={`http://csyo-az.org/${lang}/news`} />
                <link rel="canonical" href={`http://csyo-az.org/${lang}/news`} />
            </Helmet>
        );
    }
    render(){
        const HeaderImage = "http://csyo-az.org/wp-content/uploads/2016/08/Biz%C9%99-qo%C5%9Ful.jpg";
        const lang = this.props.match.params.lang;
        const allnews = this.props.allnews;

        let HomeTitle;
        let PageTitle;
        let redmoreText;

        if(lang === "en"){
            HomeTitle = GlobalTranslation.GlobalEN.HomeTitle;
            PageTitle = GlobalTranslation.GlobalEN.AllNewsTitle;
            redmoreText = GlobalTranslation.GlobalEN.redmoreText;
        }else{
            HomeTitle = GlobalTranslation.GlobalAZ.HomeTitle;
            PageTitle = GlobalTranslation.GlobalAZ.AllNewsTitle;
            redmoreText = GlobalTranslation.GlobalAZ.redmoreText;
        }

        return(
          <div className="contact-page">
              {this.head()}
              <div className="contact-header" style={{backgroundImage: `url(${HeaderImage})`}}>
                  <div className="container">
                      <h1>{PageTitle}</h1>
                      <ul className="breadcrumb">
                          <li><a href={"/" + lang}>{HomeTitle}</a></li>
                          <li>{PageTitle}</li>
                      </ul>
                  </div>
              </div>
              <div className="contact-content">
                  <div className="container">
                      <div className="row">
                          <NewsGrid buttonText={redmoreText} allnews={allnews} lang={lang}/>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

function mapStateToProps(globalState) {
    return {
        allnews: globalState.allnews
    };
}

export default connect(mapStateToProps, {getAllNews})(AllNews);