import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Menu from '../translation/Menu';
import {HeaderTranslation} from '../translation/Fields';

class Header extends Component{
    constructor(props) {
        super(props);
        this.state = {
            mobilemenu: false,
            submenu: null
        };
        this.changeLang = this.changeLang.bind(this);
        this.MobileMenu = this.MobileMenu.bind(this);
        this.openSub = this.openSub.bind(this);
    }
    changeLang(lang){
        const currentPath = this.context.router.route.location.pathname;
        const newPath = currentPath.replace(currentPath.split('/')[1], lang);
        window.location.href = newPath;
    }
    MobileMenu(){
        this.setState({mobilemenu: !this.state.mobilemenu})
    }
    openSub(pid){
        this.setState({submenu: pid});
    }
    render(){
        const currentPath = this.context.router.route.location.pathname;
        const lang = currentPath.split('/')[1];
        let HeaderMenu;
        let HeaderFields;
        if(lang === "en"){
            HeaderMenu = Menu.MenuEN;
            HeaderFields = HeaderTranslation.HeaderFieldEN;
        }else{
            HeaderMenu = Menu.MenuAz;
            HeaderFields = HeaderTranslation.HeaderFieldAZ;
        }
        return(
            <header>
                <div className="top-block ptb-15">
                    <div className="container">
                        <div className="row">
                            <div className="slogan-part col-xs-6 col-sm-6 col-md-7">
                                <p>
                                    {HeaderFields.slogan}
                                </p>
                            </div>
                            <div className="social-part col-xs-6 col-sm-6 col-md-5">
                                <div className="social-link__block text-right">
                                    <a href="https://www.facebook.com/sdgt.csyo" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook"></i></a>
                                    <a href="https://twitter.com/SDGTCSYO" target="_blank" rel="noopener noreferrer"><i className="fa fa-twitter"></i></a>
                                    <a href="https://www.youtube.com/user/SDGTcsyo" target="_blank" rel="noopener noreferrer"><i className="fa fa-youtube-play"></i></a>
                                    <a href="https://www.instagram.com/sdgt.csyo" target="_blank" rel="noopener noreferrer"><i className="fa fa-instagram"></i></a>
                                    <a href="mailto: office@csyo-az.org"><i className="fa fa-envelope"></i></a>
                                    <a role="button" className="lang" onClick={() => this.changeLang("az")}>AZ</a>
                                    <a role="button" className="lang second" onClick={() => this.changeLang("en")}>EN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="middel-part__block">
                    <div className="container">
                        <div className="row">
                            <div className="mobile-plr col-xs-10 col-sm-5 col-md-3 col-lg-4">
                                <div className="logo d-flex justify-content-center justify-content-lg-start">
                                    <a href={"/" + lang}> <img src={HeaderFields.logo} alt="SDGT - CSYO" /> </a>
                                </div>
                            </div>
                            <div className="col-xs-2 col-sm-7 col-md-9 col-lg-8">
                                <div className="top-info__block text-right">
                                    <button className="btn btn-link menu-bar" onClick={this.MobileMenu}>
                                        {this.state.mobilemenu ? (
                                            <i className="fa fa-times"></i>
                                        ):(
                                            <i className="fa fa-bars"></i>
                                        )}
                                    </button>
                                    <ul>
                                        <li className="address">
                                            <i className="fa fa-map-marker"></i>
                                            <p>
                                                {HeaderFields.addressFL} <span>{HeaderFields.addressSL}</span>
                                            </p>
                                        </li>
                                        <li className="phone">
                                            <i className="fa fa-phone"></i>
                                            <p>
                                                +994 55 823 72 90 <span>office@csyo-az.org</span>
                                            </p>
                                        </li>
                                    </ul>
                                    <a className="btn-join" href={"/" + lang+ "/page/join-us"}>
                                        {HeaderFields.JoinText}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="navbar">

                    <nav className="navbar navbar-toggleable-md navbar-sticky bootsnav">
                        <div className="container">
                            <div className="navbar-header clearfix">
                                <button className="navbar-toggler navbar-toggler-right" type="button" id="menu_toggler">
                                    <i className="fa fa-bars"></i>
                                </button>
                            </div>
                            <div className="collapse navbar-collapse mobile__nav">
                                <ul className="nav navbar-nav">
                                    {
                                        HeaderMenu.map((menu) =>
                                            <li key={menu.url}>
                                                <a href={"/" + lang + "/" + menu.url}>{menu.title}</a>
                                                {menu.children &&
                                                <ul className="dropdown-menu">
                                                    {menu.children.map((children) =>
                                                        <li key={children.url}>
                                                            <a href={"/" + lang + "/" + children.url}>{children.title}</a>
                                                            {children.children &&
                                                            <ul className="deep-menu">
                                                                {children.children.map((dchildren) =>
                                                                    <li key={dchildren.url}>
                                                                        <a href={"/" + lang + "/" + dchildren.url}>{dchildren.title}</a>
                                                                    </li>
                                                                )}
                                                            </ul>
                                                            }
                                                        </li>
                                                    )}
                                                </ul>
                                                }
                                            </li>
                                        )}
                                </ul>

                            </div>
                        </div>
                    </nav>
                </div>
                <div className={this.state.mobilemenu ? 'mobile-menu open' : 'mobile-menu'}>
                    <ul className="mobile-nav">
                        {
                            HeaderMenu.map((menu) =>
                                <li key={menu.url}>
                                    <a href={"/" + lang + "/" + menu.url}>{menu.title}</a>
                                    {menu.children && this.state.submenu !== menu.parentID &&
                                    <i className="fa fa-plus" onClick={()=> this.openSub(menu.parentID)}></i>
                                    }
                                    {menu.children && this.state.submenu && this.state.submenu === menu.parentID &&
                                    <i className="fa fa-times" onClick={()=> this.openSub(null)}></i>
                                    }
                                    {menu.children &&
                                    <ul className={this.state.submenu === menu.parentID ? 'submenu open' : 'submenu'}>
                                        {menu.children.map((children) =>
                                            <li key={children.url}>
                                                <a href={"/" + lang + "/" + children.url}>{children.title}</a>
                                            </li>
                                        )}
                                    </ul>
                                    }
                                </li>
                            )}
                    </ul>
                    <a className="btn-join" href={"/" + lang+ "/page/join-us"}>
                        {HeaderFields.JoinText}
                    </a>
                </div>
            </header>
        );
    }
}

Header.contextTypes = {
    router: PropTypes.object.isRequired
};
export default Header;