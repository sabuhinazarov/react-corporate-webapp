import React, {Component} from 'react';

class PeopleGrid extends Component{
    renderPeople(peoples){
        const lang = this.props.lang;
        return peoples.map((people, index) => {
            return(
                <div className="col-sm-6 col-md-4 col-lg-4 mb-2" key={index}>
                    <div className="people-card">
                        <img src={people.photo} alt={people.name} className="img-fluid" />
                            {lang === "en" ? (
                            <div className="item-title">
                                <h3 className="title title-bold color-light hover-yellow">
                                    <a href={"/" + lang + "/" + people.urlpath}>{people.name_en}</a>
                                </h3>
                                <div className="title-light size-md text-left color-light">{people.position_en}</div>
                            </div>
                            ):(
                                <div className="item-title">
                                    <h3 className="title title-bold color-light hover-yellow">
                                        <a href={"/" + lang + "/" + people.urlpath}>{people.name}</a>
                                    </h3>
                                    <div className="title-light size-md text-left color-light">{people.position}</div>
                                </div>
                            )}
                            <div className="item-social">
                                <ul>
                                    {people.facebook &&
                                    <li>
                                        <a href={people.facebook} title="facebook" target="_blank">
                                            <i className="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    }
                                    {people.twitter &&
                                    <li>
                                        <a href={people.twitter} title="twitter" target="_blank">
                                            <i className="fa fa-twitter" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    }
                                    {people.linkedin &&
                                    <li>
                                        <a href={people.linkedin} title="linkedin" target="_blank">
                                            <i className="fa fa-linkedin" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    }
                                    {people.email &&
                                    <li>
                                        <a href={"mailto:" + people.email} title="email">
                                            <i className="fa fa-envelope" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    }
                                </ul>
                            </div>
                    </div>
                </div>
            );
        });
    }
    render(){
        return(
            <div className="row">
                {this.renderPeople(this.props.members)}
            </div>
        );
    }
}

export default PeopleGrid;