import React, {Component} from 'react';
import Moment from "moment/moment";

class NewsGrid extends Component{
    renderNews(allnews){
        return allnews.map((content, index) => {
            return(
                <div className="col-md-4 mb-xs-30" key={content.urlpath}>
                    <div className="blog-post img-scale">
                        <div className="post-media mb-0">
                            <img src={content.imageUrl} className="img-responsive" alt={content.title} />
                            <span className="event-calender blog-date"> {Moment(content.created).format('DD')} <span>{Moment(content.created).format('MMM')}</span> </span>
                        </div>
                        <div className="post_wrap">
                            <div className="post-header">
                                {this.props.lang === "en" ? (
                                    <h4><a href={content.urlpath}>{content.title_en}</a></h4>
                                ):(
                                    <h4><a href={content.urlpath}>{content.title}</a></h4>
                                )}
                            </div>
                            <div className="post-entry">
                                {this.props.lang === "en" ? (
                                    <p>
                                        {content.content_en.replace(/(<([^>]+)>)/ig,"").substr(0, 110)}
                                    </p>
                                ):(
                                    <p>
                                        {content.content.replace(/(<([^>]+)>)/ig,"").substr(0, 110)}
                                    </p>
                                )}
                            </div>
                            <div className="post-more-link">
                                <a href={content.urlpath} className="btn-text">{this.props.buttonText}</a>
                            </div>
                        </div>
                    </div>
                </div>
            );
        });
    }
    render(){
        return(
            <div className="row blog-section">
                {this.renderNews(this.props.allnews)}
            </div>
        );
    }
}

export default NewsGrid;