import React, { Component } from 'react';

class InstaCollage extends Component {
    render() {
        return(
            <section id="instagram">
                {this.props.images.map((img, index) =>
                    <a href={"https://www.instagram.com/p/" + img.node.shortcode} target="_blank">
                        <img src={img.node.display_url} alt="" key={index}/>
                    </a>
                )}
            </section>
        );
    }
}

export default InstaCollage;