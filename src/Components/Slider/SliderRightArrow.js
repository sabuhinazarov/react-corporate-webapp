import React from 'react'

const SliderRightArrow = ({ nextSlide }) => {
  return (
    <div className="slider-right-arrow" onClick={nextSlide}>
      <i className="fa fa-chevron-right fa-2x"></i>
    </div>
  )
};

export default SliderRightArrow
