import React from 'react'

const SliderLeftArrow = ({ prevSlide }) => {
  return (
    <div className="slider-left-arrow" onClick={prevSlide}>
      <i className="fa fa-chevron-left fa-2x"></i>
    </div>
  )
};

export default SliderLeftArrow
