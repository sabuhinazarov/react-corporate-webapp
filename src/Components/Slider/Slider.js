import React, { Component } from 'react'

import Slide from './Slide'
import Dots from './Dots'
import SliderLeftArrow from './SliderLeftArrow'
import SliderRightArrow from './SliderRightArrow'

class Slider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      translateValue: 0,
      autoplay: false
    };
    this.setIndex = this.setIndex.bind(this);
    this.setTranslateValue = this.setTranslateValue.bind(this);
    this.renderSlides = this.renderSlides.bind(this);
    this.goToNextSlide = this.goToNextSlide.bind(this);
    this.goToPreviousSlide = this.goToPreviousSlide.bind(this);
    this.handleDotClick = this.handleDotClick.bind(this);
    this.slideWidth = this.slideWidth.bind(this);
  }

  setIndex (index){
    this.setState({index: index});
  };
  setTranslateValue(value){
      this.setState({translateValue: value});
  };

  renderSlides(){
    if(this.props.lang === "en"){
        return this.props.slides.map((curr, i) => <Slide key={i} image={this.props.slides[i].imageUrl} caption={this.props.slides[i].caption_en} />)
    }else{
        return this.props.slides.map((curr, i) => <Slide key={i} image={this.props.slides[i].imageUrl} caption={this.props.slides[i].caption} />)
    }
  };

  render() {
    const index = this.state.index;

    return (
      <div className="slider">
        <div className="slider-wrapper"
          style={{
            transform: `translateX(${this.state.translateValue}px)`,
            transition: 'transform ease-out 0.45s'
          }}>
          { this.renderSlides() }
        </div>

        <Dots
          index={index}
          quantity={this.props.slides.length}
          dotClick={this.handleDotClick} />

        <SliderLeftArrow prevSlide={this.goToPreviousSlide} />
        <SliderRightArrow nextSlide={this.goToNextSlide} />
      </div>
    )
  }

  goToPreviousSlide(){
    const index = this.state.index;

    if(index === 0)
      return;

      this.setTranslateValue(this.state.translateValue + this.slideWidth());
      this.setIndex(index - 1)
  };

  goToNextSlide(){
    const index = this.state.index;

    if(index === this.props.slides.length - 1) {
      this.setTranslateValue(0);
      this.setIndex(0);
      return
    }
    this.setTranslateValue(this.state.translateValue - this.slideWidth());
    this.setIndex(index + 1)
  };

  handleDotClick (i) {
    const index = this.state.index;
    if(i === index)
      return;

    if(i > index)
      this.setTranslateValue(-(i * this.slideWidth()));
    else
      this.setTranslateValue(this.state.translateValue + ((index - i) * (this.slideWidth())));

    this.setIndex(i)
  };

  slideWidth (){
    const slide = document.querySelector('.slide');
    return slide.clientWidth
  }

}

export default Slider;
