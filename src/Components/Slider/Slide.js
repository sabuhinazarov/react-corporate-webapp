import React from 'react'

const Slide = ({ image, caption }) => {

  const styles = {
    backgroundImage: `url(${image})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 60%'
  };

  return (
      <div className="slide" style={styles}>
        <div className="container">
            <div className="caption">
                <p>{caption}</p>
            </div>
        </div>
      </div>
  );
};

export default Slide
