/*global FB*/
import React, { Component} from 'react';

class FBbox extends Component {
    render() {
        return (
            <div className="fb-page" data-href="https://www.facebook.com/sdgt.csyo" data-tabs="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/sdgt.csyo" className="fb-xfbml-parse-ignore">
                    <a href="https://www.facebook.com/sdgt.csyo">Sağlam Düşüncə Gənclər Təşkilatı • SDGT</a>
                </blockquote>
            </div>
        );
    }
}
export default FBbox;