import React, { Component } from 'react';

class LoadingComponent extends Component {
    render() {
        return(
            <div className="fullpage-loader">
                <div className="image">
                    <img src="/images/loading-2.png" alt="loading..."/>
                    {/*<div className="spin spinner"></div>*/}
                </div>
            </div>
        );
    }
}

export default LoadingComponent;