import React, {Component} from 'react';
import FBbox from './FBbox';
import PropTypes from 'prop-types';
import {FooterTranslation} from '../translation/Fields';

class Footer extends Component{

    render(){
        const currentPath = this.context.router.route.location.pathname;
        const lang = currentPath.split('/')[1];
        let FooterFields;
        if(lang === "en"){
            FooterFields = FooterTranslation.FooterFieldsEN;
        }else{
            FooterFields = FooterTranslation.FooterFieldsAZ;
        }
        return(
            <footer className="footer pt-2">
                <div className="container">
                    <div className="row footer-info mb-1">
                        <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-2">
                            <h4 className="mb-2 text-uppercase">{FooterFields.contactTitle}</h4>
                            <address>
                                <i className="fa fa-map-marker"></i>{FooterFields.address}
                            </address>
                            <ul className="link-small">
                                <li>
                                    <a href="mailto:office@csyo-az.org"><i className="fa fa-envelope"></i>office@csyo-az.org</a>
                                </li>
                                <li>
                                    <a><i className="fa fa-phone"></i>(+994 55) 823 72 90</a>
                                </li>
                            </ul>
                            <div className="icons-hover-black">
                                <a href="https://www.facebook.com/sdgt.csyo" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/SDGTCSYO" target="_blank" rel="noopener noreferrer"><i className="fa fa-twitter"></i></a>
                                <a href="https://www.youtube.com/user/SDGTcsyo" target="_blank" rel="noopener noreferrer"><i className="fa fa-youtube-play"></i></a>
                                <a href="https://www.instagram.com/sdgt.csyo" target="_blank" rel="noopener noreferrer"><i className="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-2">
                            <h4 className="mb-2 text-uppercase">{FooterFields.MissionTitle}</h4>
                            <p>
                                {FooterFields.MissionContent}
                            </p>
                        </div>
                        <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-2">
                            <h4 className="mb-2 text-uppercase">Facebook</h4>
                            <FBbox/>
                        </div>
                        <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <h4 className="mb-2 text-uppercase">Instagram</h4>
                            <div className="iframe-box">
                                <iframe src="http://instafeed.sabuhi.com/getfeed/sdgt.csyo" frameBorder="0" scrolling="no" title="instafeed"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="copyright">
                    <div className="container">
                        <p className="">
                            <a><b>{FooterFields.brandName}</b></a> © 2018. {FooterFields.copyright}
                        </p>
                        <p className="">
                            Powered by <a href="https://sabuhi.com" target="_blank" rel="noopener noreferrer"><b>Sabuhi.com</b></a>
                        </p>
                    </div>
                </div>
            </footer>
        );
    }
}

Footer.contextTypes = {
    router: PropTypes.object.isRequired
};

export default Footer;