import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { Helmet } from 'react-helmet';

class NotFound extends Component {

    head() {
        return (
            <Helmet>
                <title>404 - Page Not Found</title>
                <meta property="og:title" content="404 - Page Not Found" />
                <meta name="twitter:title" content="404 - Page Not Found" />
            </Helmet>
        );
    }

    render() {
        return(
            <div className="not-found">
                {/*{this.head()}*/}
                <div className="container">
                    <h1>Səhifə tapılmadı</h1>
                    <p>Axtardığınız səhifə tapılmadı. Zəhmət olmazsa başqa səhifələrə keçin və ya Ana səhifəyə qayıdın</p>
                    <Link to="/" className="btn btn-text">Ana səhifəyə qayıt</Link>
                </div>
            </div>
        );
    }
}

export default NotFound;