import React, { Component} from 'react';

class TwitterBox extends Component {
    render() {
        return (
            <a className="twitter-timeline"  href="https://twitter.com/SDGTCSYO" data-widget-id="560773303971753985">Tweets by @SDGTCSYO</a>
        );
    }
}
export default TwitterBox;
