import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { Helmet } from 'react-helmet';

class Error extends Component {

    head() {
        return (
            <Helmet>
                <title>400 - Something Went Wrong</title>
                <meta property="og:title" content="400 - Something Went Wrong" />
                <meta name="twitter:title" content="400 - Something Went Wrong" />
            </Helmet>
        );
    }

    render() {
        return(
            <div className="not-found">
                {/*{this.head()}*/}
                <div className="container">
                    <h1>Səhvlik baş verdi</h1>
                    <p>Bilinməyən bir səbəbdən səhvlik baş verdi. Zəhmət olmazsa yenidən yoxlayın və ya başqa səhifəyə keçin</p>
                    <Link to="/" className="button no-style">Ana səhifəyə qayıt</Link>
                </div>
            </div>
        );
    }
}

export default Error;