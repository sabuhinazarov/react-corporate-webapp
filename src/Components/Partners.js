import React, {Component} from 'react';
import Slider from 'react-slick';

class Partners extends Component{

    renderPartner(partners){
        return partners.map((partner, index) => {
            return (
                <div key={index}>
                    <a href={partner.url} target="_blank">
                        <img src={partner.imageUrl} alt={partner.title} title={partner.title}/>
                    </a>
                </div>
            );
        })
    }
    render(){
        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1,
            swipeToSlide: true,
            lazyLoad: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 590,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        };
        return(
            <div className="partners">
                <div className="container">
                    <h2> {this.props.title}</h2>
                    <div className="header-line"></div>
                    <Slider {...settings}>
                        {this.renderPartner(this.props.partners)}
                    </Slider>
                </div>
            </div>
        );
    }
}

export default Partners;