import React, {Component} from 'react';
import Moment from 'moment';
import localization from 'moment/locale/az';

export default class NewsBlock extends Component{
    render(){
        const contents = this.props.contents;
        Moment.locale(this.props.lang);
        return(
            <div className="blog-section">
                <div className="container">
                    <div className="row pb-2 text-center">
                        <div className="col-sm-12">
                            <div className="creative_heading">
                                <a href={"/" + this.props.lang + this.props.preurl}>
                                    <h2>{this.props.title}</h2>
                                </a>
                                <div className="header-line"></div>
                            </div>

                        </div>
                    </div>
                    <div className="row">
                        {contents.map((content) =>
                            <div className="col-xs-12 col-sm-6 col-md-4 mb-2" key={content.urlpath}>
                                <div className="blog-post img-scale">
                                    <div className="post-media mb-0">
                                        <img src={content.imageUrl} className="img-responsive" alt={content.title} />
                                        <span className="event-calender blog-date"> {Moment(content.created).format('DD')} <span>{Moment(content.created).format('MMM')}</span> </span>
                                    </div>
                                    <div className="post_wrap">
                                        <div className="post-header">
                                            {this.props.lang === "en" ? (
                                                <h4><a href={"/" + this.props.lang + "/" +content.urlpath}>{content.title_en.substr(0, 60)}</a></h4>
                                            ):(
                                                <h4><a href={"/" + this.props.lang + "/" +content.urlpath}>{content.title.substr(0, 60)}</a></h4>
                                            )}
                                        </div>
                                        <div className="post-entry">
                                            {this.props.lang === "en" ? (
                                                <p>
                                                    {content.content_en.replace(/(<([^>]+)>)/ig,"").substr(0, 110)}...
                                                </p>
                                            ):(
                                                <p>
                                                    {content.content.replace(/(<([^>]+)>)/ig,"").substr(0, 110)}...
                                                </p>
                                            )}
                                        </div>
                                        <div className="post-more-link">
                                            <a href={"/" + this.props.lang + "/" +content.urlpath} className="btn-text">{this.props.buttonText}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}