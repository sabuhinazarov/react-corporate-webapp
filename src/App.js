import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import Header from './Components/Header';
import Footer from './Components/Footer';
import Home from './Pages/Home';
import Static from './Pages/Static';
import Team from './Pages/Team';
import Member from './Pages/Member';
import Contact from './Pages/Contact';
import News from './Pages/News';
import AllNews from './Pages/AllNews';
import Announcements from './Pages/Announcements';
import Announce from './Pages/Announce';
import NotFound from './Pages/NotFound';

class App extends Component {
  render() {
    return (
        <Router>
            <div>
                <Header/>
                <Switch>
                    <Route exact path="/" render={() => (
                        <Redirect to="/az"/>
                    )}/>
                    <Route exact path="/:lang" component={Home} />
                    <Route exact path="/:lang/page/:urlpath" component={Static} />
                    <Route exact path="/:lang/news" component={AllNews} />
                    <Route exact path="/:lang/news/:urlpath" component={News} />
                    <Route exact path="/:lang/announcements" component={Announcements} />
                    <Route exact path="/:lang/announce/:urlpath" component={Announce} />
                    <Route exact path="/:lang/team/:urlpath" component={Team} />
                    <Route exact path="/:lang/member/:urlpath" component={Member} />
                    <Route exact path="/:lang/contact" component={Contact} />
                    <Route component={NotFound} />
                </Switch>
                <Footer/>
            </div>
        </Router>
    );
  }
}

export default App;
