const Menu = {
    MenuAz : [
        {
            title: "ANA SƏHİFƏ",
            url: ""
        },
        {
            title: "Haqqımızda",
            url: "page/about-us",
            parentID: 1,
            children: [
                {
                    title: "MİSSİYAMIZ",
                    url: "page/our-mission"
                },
                {
                    title: "İDARƏ HEYƏTİ",
                    url: "team/board"
                },
                {
                    title: "KATİBLİK",
                    url: "team/secretariat"
                }
            ]
        },
        {
            title: "NATİQLİK",
            url: "page/rhitorica",
            parentID: 2,
            children: [
                {
                    title: "Debat NƏDİR?",
                    url: "page/what-is-debate"
                },
                {
                    title: "Debatın formatları",
                    url: "page/debate-formats"
                },
                {
                    title: "NATİQ ol!",
                    url: "page/become-a-speaker"
                }
            ]
        },
        {
            title: "Sağlam yaşa",
            url: "page/live-healthy",
            parentID: 3,
            children: [
                {
                    title: "Tütünə yox!",
                    url: "page/no-smoking"
                }
            ]
        },
        {
            title: "Yaşıl Düşüncə",
            url: "page/green-thinking"
        },
        {
            title: "Beynəlxalq",
            url: "page/international",
            parentID: 4,
            children: [
                {
                    title: "Erasmus+",
                    url: "page/erasmus",
                    parentID: 5,
                    children: [
                        {
                            title: "Avropa Könüllülük Xidməti",
                            url: "page/evs"
                        },
                        {
                            title: "Gənclər Mübadiləsi",
                            url: "page/youth-exchange"
                        }
                    ]
                },
                {
                    title: "Şəbəkə",
                    url: "page/network"
                },
                {
                    title: "Şərq Tərəfdaşlığı və VİŞEQRAD 4",
                    url: "page/eap-and-visegrad-4"
                }
            ]
        },
        {
            title: "Əlaqə",
            url: "contact"
        }
    ],
    MenuEN : [
        {
            title: "Home",
            url: ""
        },
        {
            title: "About Us",
            url: "page/about-us",
            parentID: 1,
            children: [
                {
                    title: "Our Mission",
                    url: "page/our-mission"
                },
                {
                    title: "Board",
                    url: "team/board"
                },
                {
                    title: "Secretariat",
                    url: "team/secretariat"
                }
            ]
        },
        {
            title: "Rhitorica",
            url: "page/rhitorica",
            parentID: 2,
            children: [
                {
                    title: "What is Debate?",
                    url: "page/what-is-debate"
                },
                {
                    title: "Debate Formats",
                    url: "page/debate-formats"
                },
                {
                    title: "Become a Speaker!",
                    url: "page/become-a-speaker"
                }
            ]
        },
        {
            title: "Live Healthy",
            url: "page/live-healthy",
            parentID: 3,
            children: [
                {
                    title: "No Smoking!",
                    url: "page/no-smoking"
                }
            ]
        },
        {
            title: "Green Thinking",
            url: "page/green-thinking"
        },
        {
            title: "International",
            url: "page/international",
            parentID: 4,
            children: [
                {
                    title: "Erasmus+",
                    url: "page/erasmus",
                    parentID: 5,
                    children: [
                        {
                            title: "EVS",
                            url: "page/evs"
                        },
                        {
                            title: "Youth Exchange",
                            url: "page/youth-exchange"
                        }
                    ]
                },
                {
                    title: "Network",
                    url: "page/network"
                },
                {
                    title: "EaP & VISEGRAD 4",
                    url: "page/eap-and-visegrad-4"
                }
            ]
        },
        {
            title: "Contact",
            url: "contact"
        }
    ]
};

export default Menu;