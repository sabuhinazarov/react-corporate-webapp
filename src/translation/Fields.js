export const GlobalTranslation = {
    GlobalAZ : {
        HomeTitle : "Ana səhifə",
        AllNewsTitle: "Xəbərlər",
        AnnouncementsTitle: "Elanlar",
        redmoreText: "Ardını oxu",
        loadmoreText: "Daha çox",
        brandName : "\"Sağlam Düşüncə\" Gənclər Təşkilatı",
        address: "5-ci MKR, Sumqayıt Gənclər Evi, Sumqayıt şəh. Azərbaycan AZ5007.",
        published: "Dərc edilib"
    },
    GlobalEN : {
        HomeTitle : "Home Page",
        AllNewsTitle: "All News",
        AnnouncementsTitle: "Announcements",
        redmoreText: "Read more",
        loadmoreText: "Load more",
        brandName : "\"Common Sense\" Youth Organization",
        address: "5 mic/dist, Sumgait Youth House, Sumgait city, Azerbaijan, AZ5011.",
        published: "Published"
    }
};
export const HeaderTranslation = {
    HeaderFieldAZ : {
        JoinText : "BİZƏ QOŞUL",
        slogan: "Gənclərlə dəyişikliyə doğru!",
        logo : "/images/logo-az.png",
        addressFL: "5-ci MKR, Sumqayıt Gənclər Evi",
        addressSL: "Sumqayıt şəh. Azərbaycan AZ5007"
    },
    HeaderFieldEN : {
        JoinText : "Join Us",
        slogan: "With youth towards charge!",
        logo : "/images/logo-en.png",
        addressFL: "5 mic/dist, Sumgait Youth House",
        addressSL: "Sumgait city, Azerbaijan, AZ5011"
    }
};

export const FooterTranslation = {
    FooterFieldsAZ : {
        contactTitle : "Əlaqə",
            address: "5-ci MKR, Sumqayıt Gənclər Evi, Sumqayıt şəh. Azərbaycan AZ5007.",
            MissionTitle : "Missiyamız",
            MissionContent : "\"Sağlam Düşüncə\" Gənclər Təşkilatı 2006-ci ildə Sumqayıt şəhərində bir qrup gənc tərəfindən təsis edilmiş qeyri-siyası, qeyri-kommersiya, qeyri-hökumət gənclər qurumudur. Missiyamız vətədaş cəmiyyətinin inkişafında gənclərin iştirakının artırılmasıdır.",
            brandName : "\"Sağlam Düşüncə\" Gənclər Təşkilatı",
            copyright: "Bütün Hüquqlar Qorunur."
    },
    FooterFieldsEN : {
        contactTitle : "Contact",
            address: "5 mic/dist, Sumgait Youth House, Sumgait city, Azerbaijan, AZ5011.",
            MissionTitle : "Our Mission",
            MissionContent : "\"Common Sense\" Youth Organization is a non-governmental, non-political and non-commercial organization which was founded by a group of young people in 2006. Our mission is to increase the participation of the youth in the development of civil society.",
            brandName : "\"Common Sense\" Youth Organization",
            copyright: "All Rights Reserved."
    }
};

export const HomeTranslation = {
    HomeAZ : {
        newsBlockTitle: "Xəbərlər",
        redmoreText: "Ardını oxu",
        galleryTitle: "Qalereya",
        annBlockTitle: "Elanlar",
        partnersTitle: "Tərəfdaşlar"
    },
    HomeEN: {
        newsBlockTitle: "News",
        redmoreText: "Read more",
        galleryTitle: "Gallery",
        annBlockTitle: "Announcements",
        partnersTitle: "Partners"
    }
};

export const TeamTranslation = {
    TeamAZ : {
        boardTitle: "İdarə Heyəti",
        secretariatTitle: "Katiblik"
    },
    TeamEN : {
        boardTitle: "Board Members",
        secretariatTitle: "Secretariat"
    }
};

export const ContactTranslation = {
    AZ : {
        PageTitle: "Bizimlə Əlaqə",
        contactForm: "Əlaqə formu",
        contactInfo: "Əlaqə məlumatları",
        buttonText: "Göndər",
        name: "Adınız",
        subject: "Mövzu",
        message: "Mesajınız"
    },
    EN : {
        PageTitle: "Contact Us",
        contactForm: "Contact Form",
        contactInfo: "Contact Info",
        buttonText: "Send",
        name: "Full Name",
        subject: "Subject",
        message: "Message"
    }
};